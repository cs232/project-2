### Parts of tutorial I don't get
https://steve-parker.org/sh/bourne.shtml

- 2.1 Control flow - for
- 2.3 Here documents
- 2.4 Shell variables
- 2.8 Command grouping (remember)
- 2.9 Debugging shell procedures (remember)
- 3.1 Parameter transmission
- 3.2 Parameter substitution (remember)
- 3.3 Command substitution (remember)
- 3.4 Evaluation and quoting (remember)
- 3.5 Error handling (remember)
- 3.6 Fault handling
- 3.7 Command execution (learn more detail)
- 3.8 Invoking the shell (remember)
