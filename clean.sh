#!/bin/sh
# Project 2: Shell Scripts
# 3/12/19
# Catherine DeJager, for CS 232 at Calvin College

# clean a given directory, removing:
# core files, emacs backup files (those ending in ~),
# emacs auto-save files (those beginning and ending in #),
# intermediate object files (those ending in .o), and
# executable files (these can be easily recreate when neededd, especially if the directory contains a Makefile)
# note: this function does cd into the given directory
# parameters: directory name (relative or absolute),
#   iflag: if this is 1, ask the user if they want to delete a given file (remove the file only if the user replies y),
#   qflag: if this is 1, then the names of "extraneous" files are not echoed to the screen
cleanDir() {
  DIR=$1
  iflag=${2:-0}  # second argument or 0 if second argument is not provided
  qflag=${3:-0}  # third argument or 0 if third argument is not provided
  predir=$PWD  # get the previous directory so we can go back when we're done
  cd $DIR
  for f in $(ls $PWD)
  do
    if [ $f = "core" ]
    then
      if [ $qflag -eq 0 ]
      then
        echo "core file $f"
      fi
      if [ $iflag -eq 1 ]
      then
        rm -i $f
      else
        rm $f
      fi
    elif [ $f = "Makefile" ]
    then
      if [ $qflag -eq 0 ]
      then
        echo "makefile $f"
      fi
      if [ $iflag -eq 1 ]
      then
        rm -i $f
      else
        rm $f
      fi
    elif echo "$f" | grep -q "~$"  # -q for quiet so grep doesn't output result
    then
      if [ $qflag -eq 0 ]
      then
        echo "emacs backup file $f"
      fi
      if [ $iflag -eq 1 ]
      then
        rm -i $f
      else
        rm $f
      fi
    elif echo $f | grep -q "#.*#\$"
    then
      if [ $qflag -eq 0 ]
      then
        echo "emacs auto-save file $f"
      fi
      if [ $iflag -eq 1 ]
      then
        rm -i $f
      else
        rm $f
      fi
    elif echo $f | grep -q "*.o"
    then
      if [ $qflag -eq 0 ]
      then
        echo "intermediate object file $f"
      fi
      if [ $iflag -eq 1 ]
      then
        rm -i $f
      else
        rm $f
      fi
    elif [ -d "$PWD/$f" ]
    then
      if [ $qflag -eq 0 ]
      then
        echo "directory $f at $PWD/$f"
      fi
      cleanDir "$PWD/$f" $iflag $qflag  # count the number of files removed by recursive call
    elif ! echo $f | grep -q "\."  # Executable fies have no file extension and thus no .
    then
      if [ $qflag -eq 0 ]
      then
        echo "executable file $f"
      fi
      if [ $iflag -eq 1 ]
      then
        rm -i $f
      else
        rm $f
      fi
    fi
  done
  cd ..  # go back to parent directory
}

# main code
if [ $# -eq 0 ]  # if no arguments are supplied
then
  echo "No arguments supplied. Using working directory."; D="$PWD"
elif [ -d $1 ]
then
  D=$1
  # get the full path to the specified directory, because absolute paths are nicer to work with than relative, especially when going into subdirectories
  D="$(realpath $D)"
  shift  # get rid of the first argument
else
  echo "Invalid directory name supplied: {$1}. Using working directory."; D="$PWD"
fi

# adapted from http://www.shelldorado.com/goodcoding/cmdargs.html
iflag=0
qflag=0
sflag=0
set -- `getopt -o iqs --long "interactive,quiet,stats" -- "$@"`  # use getopt to get options
[ $# -lt 1 ] && exit 1  # getopt failed
while [ $# -gt 0 ]
do
  case "$1" in
    -i|--interactive)   iflag=1;;
    -q|--quiet)         qflag=1;;
    -s|--stats)         sflag=1;;
    *)                  break;;  # terminate while loop
  esac
  shift
done
# all command line switches are processed

# adapted from https://superuser.com/questions/121627/how-to-get-elements-from-list-in-bash
List=$(du -s $D)  # get disk space before cleaning directory
set -- $List  # now $1 contains the disk usage and $2 contains the directory names
usagepre=$1
# find all files in the given directory and count the number of files
filespre=$(find "$D" -type f | wc -l)


cleanDir $D $iflag $qflag
if [ $sflag -eq 1 ]
then
  echo $PWD
  filespost=$(find "$D" -type f | wc -l)
  echo "$filespre - $filespost = $((filespre-filespost)) files removed (calls to find)"
  List=$(du -s $D)
  set -- $List  # now $1 contains the disk usage and $2 contains the directory names
  usagepost=$1  # TODO: figure out why usagepost is the same as usagepre even though running du on terminal indicates disk usage changed
  echo "Disk usage before: $usagepre. After: $usagepost. ($((usagepre-usagepost)) reclaimed)"
fi
